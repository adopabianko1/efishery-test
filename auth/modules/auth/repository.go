package auth

import (
	"database/sql"
	"log"

	"github.com/adopabianko/efishery/database"
	"github.com/adopabianko/efishery/utils"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type IAuthRepository interface {
	SaveRepo(user *User) (bool, string)
	FindUserExitsByPhoneRepo(phone string) (bool, User)
	FindUserRegisteringRepo(id uuid.UUID) (User, error)
}

type AuthRepository struct {
	MySQL database.IMySQLConnection
}

func (r *AuthRepository) FindUserExitsByPhoneRepo(phone string) (status bool, user User) {
	db := r.MySQL.CreateConnection()
	defer db.Close()

	err := db.QueryRow(`
		SELECT
		    name,
		   	phone,
		    role,
		    password,
		    created_at
		FROM users 
		WHERE phone = ?`, phone).Scan(
			&user.Name,
			&user.Phone,
			&user.Role,
			&user.Password,
			&user.CreatedAt,
		)

	if err != nil {
		if err == sql.ErrNoRows {
			return false, user
		}

		log.Fatal(err.Error())
	}

	return true, user
}

func (r *AuthRepository) SaveRepo(user *User) (status bool, password string) {
	db := r.MySQL.CreateConnection()
	defer db.Close()

	uuid := uuid.New()
	generateDefaultPassword := utils.PasswordGenerator()
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(generateDefaultPassword), bcrypt.MinCost)

	_, err = db.Exec(`INSERT INTO 
    		users(
    			id,
    			name,
				phone,
				role,
				password
    		) VALUES(?,?,?,?,?)`,
		uuid,
		user.Name,
		user.Phone,
		user.Role,
		hashedPassword,
	)

	if err != nil {
		log.Fatal(err.Error())
	}

	return true, generateDefaultPassword
}

func (r *AuthRepository) FindUserRegisteringRepo(id uuid.UUID) (user User, err error) {
	db := r.MySQL.CreateConnection()
	defer db.Close()

	err = db.QueryRow(`
		SELECT
			id,
			name,
			phone,
			role
		FROM users WHERE id = ?
	`, id).Scan(
		&user.ID,
		&user.Name,
		&user.Phone,
		&user.Role,
	)

	if err != nil {
		return
	}

	return user, nil
}
