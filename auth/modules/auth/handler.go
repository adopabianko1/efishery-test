package auth

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/adopabianko/efishery/utils"
	"github.com/dgrijalva/jwt-go"

	"github.com/labstack/echo"
	"github.com/xeipuuv/gojsonschema"
)

type AuthController struct {
	Service IAuthService
}

func InitAuthController() *AuthController {
	authService := InitAuthService()

	authController := new(AuthController)
	authController.Service = authService
	return authController
}

func (r *AuthController) RegisterHandler(c echo.Context) error {
	u := new(User)
	if err := c.Bind(u); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, echo.Map{
			"code":    422,
			"message": err.Error(),
		})
	}

	valueJson, err := json.Marshal(u)
	if err != nil {
		log.Fatal(err.Error())
	}

	schemaLoader := gojsonschema.NewReferenceLoader("file://./validation/register_schema.json")
	documentLoader := gojsonschema.NewStringLoader(fmt.Sprintf("%s", valueJson))

	validate, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		log.Fatal(err.Error())
	}

	if !validate.Valid() {
		for _, desc := range validate.Errors() {
			return c.JSON(419, echo.Map{
				"code":    419,
				"message": fmt.Sprintf("%s", desc),
			})
		}
	}

	httpCode, message, password := r.Service.RegisterService(u)

	if httpCode != 200 {
		return c.JSON(httpCode, echo.Map{
			"code":    httpCode,
			"message": message,
		})
	}

	return c.JSON(httpCode, echo.Map{
		"code":    httpCode,
		"message": message,
		"data": echo.Map{
			"password": password,
		},
	})
}

func (r *AuthController) GenerateTokenHandler(c echo.Context) error {
	u := new(User)
	if err := c.Bind(u); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, echo.Map{
			"code":    422,
			"message": err.Error(),
		})
	}

	valueJson, err := json.Marshal(u)
	if err != nil {
		log.Fatal(err.Error())
	}

	schemaLoader := gojsonschema.NewReferenceLoader("file://./validation/generate_token_schema.json")
	documentLoader := gojsonschema.NewStringLoader(fmt.Sprintf("%s", valueJson))

	validate, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		log.Fatal(err.Error())
	}

	if !validate.Valid() {
		for _, desc := range validate.Errors() {
			return c.JSON(419, echo.Map{
				"code":    419,
				"message": fmt.Sprintf("%s", desc),
			})
		}
	}

	httpCode, message, result := r.Service.GenerateTokenService(u.Phone, u.Password)

	if httpCode != 200 {
		return c.JSON(httpCode, echo.Map{
			"code":    httpCode,
			"message": message,
		})
	}

	accessToken := utils.CreateAccessToken(result.Name, result.Phone, result.Role, result.CreatedAt)

	return c.JSON(httpCode, echo.Map{
		"code":    httpCode,
		"message": message,
		"data": echo.Map{
			"access_token": accessToken,
		},
	})
}

func (r *AuthController) VerifyTokenHandler(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*utils.JwtCustomClaims)

	return c.JSON(200, echo.Map{
		"code":    200,
		"message": "Verify token success",
		"data":    claims,
	})
}
