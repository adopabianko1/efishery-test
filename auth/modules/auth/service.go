package auth

import (
	"github.com/adopabianko/efishery/database"
	"golang.org/x/crypto/bcrypt"
)

type IAuthService interface {
	RegisterService(user *User) (int, string, string)
	GenerateTokenService(phone, password string) (int, string, User)
}

type AuthService struct {
	Repository IAuthRepository
}

func InitAuthService() *AuthService {
	authRepository := new(AuthRepository)
	authRepository.MySQL = &database.MySQLConnection{}

	authService := new(AuthService)
	authService.Repository = authRepository

	return authService
}

func (s *AuthService) RegisterService(user *User) (httpCode int, message string, password string) {
	// Validate user is exist
	userExists, _ := s.Repository.FindUserExitsByPhoneRepo(user.Phone)
	if userExists {
		return 422, "User is exists", password
	}

	// Insert data register
	insert, password := s.Repository.SaveRepo(user)
	if !insert {
		return 500, "Register failed", password
	}

	return 200, "Register success", password
}

func (s *AuthService) GenerateTokenService(phone, password string) (httpCode int, message string, user User) {
	// Check user is exist
	userExists, user := s.Repository.FindUserExitsByPhoneRepo(phone)
	if !userExists {
		return 404, "User is not found", user
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))

	if err != nil {
		return 404, "User is not found", user
	}

	return 200, "Generate token success", user
}