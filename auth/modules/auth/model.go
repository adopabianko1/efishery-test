package auth

type User struct {
	ID       string `json:"id,omitempty"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
	Role     string `json:"role"`
	Password string `json:"password,omitempty"`
	CreatedAt string `json:"created_at,omitempty"`
}