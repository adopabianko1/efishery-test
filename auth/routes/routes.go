package routes

import (
	"fmt"
	"github.com/adopabianko/efishery/modules/auth"
	"github.com/adopabianko/efishery/utils"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
)

func Routes() {
	authController := auth.InitAuthController()

	e := echo.New()

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "remote_ip=${remote_ip}, method=${method}, uri=${uri}, status=${status}\n",
	}))
	e.Use(middleware.Recover())

	secretKey := viper.Get("jwt-key")
	configJwt := middleware.JWTConfig{
		Claims:     &utils.JwtCustomClaims{},
		SigningKey: []byte(secretKey.(string)),
	}

	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"app":     viper.Get("app.name"),
			"version": "0.0.1",
		})
	})

	e.POST("/auth/register", authController.RegisterHandler)
	e.POST("/auth/generate-token", authController.GenerateTokenHandler)
	e.GET("/auth/verify-token", authController.VerifyTokenHandler, middleware.JWTWithConfig(configJwt))

	port := fmt.Sprintf("%d", viper.Get("app.port"))

	e.Logger.Fatal(e.Start(":" + port))
}
