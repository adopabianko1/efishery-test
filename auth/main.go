package main

import (
	"log"

	"github.com/adopabianko/efishery/routes"
	"github.com/spf13/viper"
)

func init() {
	viper.AddConfigPath(".")
	viper.SetConfigName(".app-config")
	viper.SetConfigType("yaml")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err.Error())
	}
}

func main() {
	routes.Routes()
}
