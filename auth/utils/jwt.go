package utils

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
	"log"
	"time"
)

type JwtCustomClaims struct {
	Name  string `json:"name"`
	Phone  string `json:"phone"`
	Role  string `json:"role"`
	Timestamp  string `json:"timestamp"`
	jwt.StandardClaims
}

func CreateAccessToken(name, phone, role, created_at string) (tokenString string) {
	secretToken := viper.Get("jwt-key")

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &JwtCustomClaims{
		name,
		phone,
		role,
		created_at,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	})

	tokenString, err := token.SignedString([]byte(secretToken.(string)))

	if err != nil {
		log.Fatal(err.Error())
	}

	return
}