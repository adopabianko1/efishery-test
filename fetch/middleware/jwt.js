const jwt = require('jsonwebtoken');

const validateToken = (req, res, next) => {
    let token = req.headers['authorization']; 
    token = token.replace(/^Bearer\s+/, "");

    if (!token) return res.status(401).send({
        status: 401,
        message: 'Access denied'
    });

    try {
        const verified = jwt.verify(token, process.env.jwt_key);
        req.user = verified;
        next();
    } catch(err) {
        res.status(400).send({
            status: 400,
            message: 'Invalid token'
        });
    }
}

module.exports = {
    validateToken
}