const express = require('express');
const router = express.Router();
const axios = require('axios');
const {validateToken} = require('../middleware/jwt');
const currency = require('../config/currency.json');
const jwt = require('jsonwebtoken');
const lodash = require('lodash');
const moment = require('moment');

// Default route
router.get('/', function(req, res) {
  res.status(200).send({
    "app": process.env.app_name,
    "version": process.env.version
  });
});

router.get('/fetch', validateToken, async function(req, res) {
  try {
    const usd_idr = currency.USD_IDR; 
    const steinResp = await axios.get(process.env.stein_url);

    const newSteinArr = [];

    steinResp.data.forEach(el => {
      if (el.price !== null) {
        const priceUsd = parseFloat(el.price) / parseFloat(usd_idr);
        const newObj = {...el, price_usd: priceUsd.toFixed(2)};
        newSteinArr.push(newObj)
      }
    });

    res.status(200).send({
      "code": 200,
      "message": "Stein data",
      "data": newSteinArr
    });

  } catch(err) {
    res.status(500).send({
      "code": 500,
      "message": err
    })
  }
});

router.get('/fetch-admin', validateToken, async function(req, res) {
  try {
    let token = req.headers['authorization']; 
    token = token.replace(/^Bearer\s+/, "");
    
    const decoded = jwt.verify(token, process.env.jwt_key);

    if (decoded.role !== 'admin') {
      res.status(403).send({
        "code": 403,
        "message": "Not Authenticated"
      });
    }

    const steinResp = await axios.get(process.env.stein_url);

    const steinRespFiltered = steinResp.data.filter((i) => (i.area_provinsi != null && i.tgl_parsed != null && i.price != null))

    const newSteinarr = lodash.chain(steinRespFiltered)
      .groupBy('area_provinsi')
      .mapValues((i) => {
        return lodash.groupBy(i, (el) => moment(el.tgl_parsed).week())
      })
      .mapValues((weeklyData) => {
        return lodash.mapValues(weeklyData, (i) => {
          return {
            max: lodash.maxBy(i, function (el) {
              return el.price;
            }),
            min: lodash.minBy(i, function (el) {
              return el.price;
            }),
            avg: parseInt(lodash.meanBy(i, function (el) {
              return parseInt(el.price);
            }))
          }
        })
      })

    res.status(200).send({
      "code": 200,
      "message": "Stain data",
      "data": newSteinarr
    });

  } catch(err) {
    res.status(500).send({
      "code": 500,
      "message": err.message
    })
  }
});

router.get('/verify-token', validateToken, async function(req, res) {
    let token = req.headers['authorization']; 
    token = token.replace(/^Bearer\s+/, "");

    try {
      const decoded = jwt.verify(token, process.env.jwt_key);

      res.status(200).send({
        "code": 200,
        "message": "Verify token success",
        "data": decoded
      });
    } catch(err) {
      res.status(500).send({
        "code": 500,
        "message": err
      })
    }
});

module.exports = router;
