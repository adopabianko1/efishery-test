const fs = require('fs');
const axios = require('axios');

exports.SyncCurrency = async () => {
    try {
        const resp = await axios.get(process.env.currency_url);

        fs.writeFile('./config/currency.json', JSON.stringify(resp.data), (err) => {
            if (err) console.log(err);

            console.log('Succefully Sync Currency Data');
        })
    } catch(err) {
        console.log(error);
    }
}