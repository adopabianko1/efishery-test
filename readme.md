# Install via Docker

Clone project.

`git clone https://gitlab.com/adopabianko1/efishery-test.git`

Setup project Auth App.

`$ cd auth`

`$ cp -R app-config.yaml.example .app-config.yaml`

Setup project Fetch App.

`$ cd fetch`

`$ cp -R .env.example .env`

Run app via docker compose.

`$ docker compose up -d`

Setelah docker compose berjalan dengan lancar, maka Auth App akan berjalan di port 3000 dan Fetch App akan berjalan di port 3131.



# API

#### **Auth APP**

1. **Register**

   **Method** : Post

   **Url** : http://localhost:3000/auth/register

   **Request** :

   curl --request POST \
     --url http://localhost:3000/auth/register \
     --header 'Content-Type: application/json' \
     --data '{
       "name": "Naufal",
       "phone": "087844033221",
       "role": "admin"
   }'

   **Response** : 
   
   {
     "code": 200,
     "data": {
       "password": "9oy3"
     },
     "message": "Register success"
   }

   

2. **Generate Token**

   **Method** : Post

   **Url** : http://localhost:3000/auth/generate-token

   **Request** : 

   curl --request POST \
     --url http://localhost:3000/auth/generate-token \
     --header 'Content-Type: application/json' \
     --data '{
   	"phone": "087874083221",
   	"password": "9oy3"
   }'

   **Response** : 

   {
     "code": 200,
     "data": {
       "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTmF1ZmFsIiwicGhvbmUiOiIwODc4NzQwODMyMjEiLCJyb2xlIjoic3B2IiwidGltZXN0YW1wIjoiMjAyMS0wNS0wOSAxNjoyMTo1NSIsImV4cCI6MTYyMDgzNjUyNX0.DfQggvYunozREqdW1tqsmm54clB1mNxTq_RctW1Zmx8"
     },
     "message": "Generate token success"
   }

   

3. **Verify Token**

   **Method** : Get

   **Url** : http://localhost:3000/auth/verify-token

   **Request** :

   curl --request GET \
     --url http://localhost:3000/auth/verify-token \
     --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQWRvIFBhYmlhbmtvIiwicGhvbmUiOiIwODc4NzQwODMyMjAiLCJyb2xlIjoiYWRtaW4iLCJ0aW1lc3RhbXAiOiIyMDIxLTA1LTA5IDE1OjA5OjMwIiwiZXhwIjoxNjIwODMyMTgyfQ.WP4LG78lyCKn33H1fX1DiMnTxeHhrPDQo_nTPqxPZ0o' \
     --header 'Content-Type: application/json' 

   **Response** : 

   {
     "code": 200,
     "data": {
       "name": "Ado Pabianko",
       "phone": "087874083220",
       "role": "admin",
       "timestamp": "2021-05-09 15:09:30",
       "exp": 1620832182
     },
     "message": "Verify token success"
   } 

#### Fetch App

1. **Fetch Data**

   **Method :** Get

   **Url :** http://localhost:3131/fetch

   **Request :**

   curl --request GET \
     --url http://localhost:3131/fetch \
     --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQWRvIFBhYmlhbmtvIiwicGhvbmUiOiIwODc4NzQwODMyMjAiLCJyb2xlIjoiYWRtaW4iLCJ0aW1lc3RhbXAiOiIyMDIxLTA1LTA5IDE1OjA2OjAxIiwiZXhwIjoxNjIwODMxOTg1fQ.nF-HPwIDzjalJWmnlU_qkDut98orEE7rhuEue0Bj8Hk'

   **Response :** https://jsonformatter.org/0c8490

2. **Fetch Aggregate Data (Role admin)**

   **Method :** Get

   **Url :** http://localhost:3131/fetch-admin

   **Request :**

   curl --request GET \
     --url http://localhost:3131/fetch-admin \
     --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQWRvIFBhYmlhbmtvIiwicGhvbmUiOiIwODc4NzQwODMyMjAiLCJyb2xlIjoiYWRtaW4iLCJ0aW1lc3RhbXAiOiIyMDIxLTA1LTA5IDE1OjA2OjAxIiwiZXhwIjoxNjIwODMxOTg1fQ.nF-HPwIDzjalJWmnlU_qkDut98orEE7rhuEue0Bj8Hk'

   **Response :** https://jsonformatter.org/0bc029

3. **Verify Token**

   **Method :** Get

   **Url :** http://localhost:3131/verify-token

   **Request :** 

   curl --request GET \
     --url http://localhost:3131/verify-token \
     --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQWRvIFBhYmlhbmtvIiwicGhvbmUiOiIwODc4NzQwODMyMjAiLCJyb2xlIjoiYWRtaW4iLCJ0aW1lc3RhbXAiOiIyMDIxLTA1LTA5IDE1OjA5OjMwIiwiZXhwIjoxNjIwODMyMTgyfQ.WP4LG78lyCKn33H1fX1DiMnTxeHhrPDQo_nTPqxPZ0o' \
     --header 'Content-Type: application/json'

   **Response :**
   
   {
     "code": 200,
     "message": "Verify token success",
     "data": {
       "name": "Ado Pabianko",
       "phone": "087874083220",
       "role": "admin",
       "timestamp": "2021-05-09 15:09:30",
       "exp": 1620832182
     }
   }

# Design System

![System Diagram](https://user-images.githubusercontent.com/8348927/117591101-0510e400-b15d-11eb-808e-09d104bb0554.png)
